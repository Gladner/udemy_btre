# Generated by Django 4.1.6 on 2023-02-12 20:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='listing',
            name='bathrooms',
            field=models.DecimalField(decimal_places=1, default=1.0, max_digits=2),
            preserve_default=False,
        ),
    ]
